# Algorithm task

Parse input text file line by line and evaluate a string with postfix mathematical expression

![Description](description.png)

## How to execute
```
php parser.php input.txt
```

