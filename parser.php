#!/usr/bin/php
<?php
try {
    // Check input file is passed as an argument and exists
    if (!isset($argv[1])) {
        throw new Exception('Missing file argument!');
    }
    if (!file_exists($argv[1])) {
        throw new Exception('file does not exist!');
    }
    // Open file for reading
    $handle = fopen($argv[1], 'r');
    if (!$handle) {
        throw new Exception('Unable to read the file!!');
    }
    // Loop in file line by line    
    while (($line = fgets($handle)) !== FALSE) {
        if ($line = trim($line)) {
            // Loop in the same line while doing :
            // Extract first expression from line
            // Calculate the expression result
            // Replace the expression from line with result calculated
            $score = null;
            $matches = [];
            $expr = '%(-?[0-9]*\.?[0-9]+?)\s+(-?[0-9]*\.?[0-9]+?)\s+(\+|-|\*|/)%';
            while($result = preg_match($expr, $line, $matches)) {
                if ($matches[3] == '/' && $matches[2] == 0) {
                    echo 'error division by zero';
                    break;
                }
                eval ("\$score = '$matches[1]' {$matches[3]} '$matches[2]';");
                $pattern = '%'.preg_quote($matches[0]).'%';
                $line = preg_replace($pattern, $score, $line);
            }
            // Print the result after completion
            if ($score) {
            
                if (gettype($score) == "integer") {
                    echo number_format($score, 1, '.', '').PHP_EOL;
                }
                else {
                    echo floatval(round ($score, 13,PHP_ROUND_HALF_UP )) .PHP_EOL;
                }
            }
            
            
        }
        
        
    }
    // Close the opened file pointer
    fclose($handle);
    
} catch (Exception $e) {
    echo $e->getMessage().PHP_EOL;
}
